package com.qayyuum;

import java.util.ArrayList;

public class Bank {
    private String name;
    private ArrayList<Branch> branches = new ArrayList<Branch>();

    public Bank(String name) {
        this.name = name;
    }


    //Add a new branch
    //Add a customer to that branch with initial transaction
    //Add a transaction for an existing customer for that branch
    //Show a list of customers for a particular branch and optionally a list
    //of their transactions
    public boolean addBranch(Branch branch) {
        if (findBranch(branch.getName()) >= 0) {
            System.out.println("This branch is already registered.");
            return false;
        }
        this.branches.add(branch);
        return true;
    }

    public boolean addCustomer(Branch branch, Customer customer) {
        boolean addCustomerOK = branch.addCustomer(customer);
        return addCustomerOK;
    }

    private int findBranch(Branch branch) {
        return this.branches.indexOf(branch);
    }

    private int findBranch(String branchName) {
        for (int i = 0; i < this.branches.size(); i++) {
            Branch branch = this.branches.get(i);
            if (branch.getName().equals(branchName)) {
                return i;
            }
        }
        return -1;
    }

    public boolean addTransaction(String branchName, String customerName, double transaction) {
        Branch thisBranch;
        Customer thisCustomer;
        int selectedCustIdx;
        int selectedBranchIdx = findBranch(branchName);
        if (selectedBranchIdx >= 0) {
            thisBranch = this.branches.get(selectedBranchIdx);
            selectedCustIdx = thisBranch.findCustomer(customerName);
            if (selectedCustIdx >= 0) {
                thisCustomer = thisBranch.getCustomer(selectedCustIdx);
                thisBranch.addTransaction(thisCustomer, transaction);
                return true;
            }
        }
        return false;
    }

    public void printBranchList(){
        if (this.branches.size() == 0) {
            System.out.println("No branches registered under " + this.name + ".");
            return;
        }

        System.out.println("The branches for " + this.name + " are:");
        for (int i =0; i<this.branches.size(); i++) {
            System.out.println((i+1) + ". " + this.branches.get(i).getName());
        }
    }

    public Branch getBranch(String branchName) {
        int branchIdx = findBranch(branchName);
        Branch foundBranch = null;
        if (branchIdx >= 0) {
            foundBranch = this.branches.get(branchIdx);
        }
        return foundBranch;
    }

}

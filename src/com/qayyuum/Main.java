package com.qayyuum;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Bank myBank = new Bank("Maybank");
        Branch newBranch = Branch.createBranch("Rawang");
        myBank.addBranch(newBranch);
        myBank.printBranchList();

        Customer myCustomer = Customer.createCustomer("Qayyuum", 100d);
        myBank.addCustomer(newBranch, myCustomer);

        myBank.getBranch("Rawang").printCustomerList();

    }
}

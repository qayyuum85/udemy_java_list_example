package com.qayyuum;

import java.util.ArrayList;

public class Customer {
    private String name;
    private ArrayList<Double> transactions = new ArrayList<Double>();

    public Customer(String name, double transaction) {
        this.name = name;
        this.transactions.add(0,transaction);
    }

    public String getName() {
        return name;
    }

    public static Customer createCustomer(String name, double initTransaction) {
        return new Customer(name, initTransaction);
    }

    public void addTransaction(Double transaction) {
        this.transactions.add(transaction);
    }

    public void printTransaction() {
        System.out.println("This is transaction for " + this.name);
        for (int i =0; i<transactions.size(); i++) {
            System.out.println((i+1) + ". " + transactions.get(i));
        }

    }
}

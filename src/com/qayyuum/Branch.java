package com.qayyuum;

import java.util.ArrayList;

public class Branch   {
    private String name;
    private ArrayList<Customer> customers = new ArrayList<Customer>();

    public Branch(String name) {
        this.name = name;
    }

    public boolean addCustomer(Customer customer) {
        if (findCustomer(customer.getName()) >= 0) {
            System.out.println("Customer is already registered in this branch.");
            return false;
        }
        customers.add(customer);
        return true;
    }

    public void addTransaction(Customer customer, Double transaction) {
        int customerIdx = findCustomer(customer);
        if (customerIdx >= 0) {
            this.customers.get(customerIdx).addTransaction(transaction);
        }
    }

    public static Branch createBranch(String name) {
        return new Branch(name);
    }

    public String getName() {
        return name;
    }

    public int findCustomer(String customerName) {
        for (int i=0; i<this.customers.size(); i++) {
            Customer customer = this.customers.get(i);
            if (customer.getName().equals(customerName)) {
                return i;
            }
        }
        return -1;
    }

    private int findCustomer(Customer customer) {
        return customers.indexOf(customer);
    }

    public Customer getCustomer(int custIdx) {
        return this.customers.get(custIdx);
    }

    public void printCustomerList(){
        System.out.println("The customers for " + this.name + " are:");
        for (int i =0; i<this.customers.size(); i++) {
            System.out.println((i+1) + ". " + this.customers.get(i).getName());
        }
    }

}
